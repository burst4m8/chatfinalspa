drop table if exists auth;
create table auth(
                    userid bigint,
                    login varchar(255),
                    password varchar(255),
                    token varchar(255),
);

drop table if exists user;
create table user(
                      id serial,
                      name varchar(255)
);

drop table if exists chat;
create table chat(
                     id serial,
                     name varchar(255)
);

drop table if exists messages;
create table messages(
                        id serial,
                        userid bigint,
                        chatid bigint,
                        text varchar(255)
);