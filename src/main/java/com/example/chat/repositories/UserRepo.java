package com.example.chat.repositories;

import com.example.chat.models.Auth;
import com.example.chat.models.Chat;
import com.example.chat.models.Messages;
import com.example.chat.models.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface UserRepo extends JpaRepository<User,Long>{
}
