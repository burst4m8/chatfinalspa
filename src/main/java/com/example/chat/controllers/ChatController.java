package com.example.chat.controllers;

import com.example.chat.models.Chat;
import com.example.chat.repositories.AuthRepo;
import com.example.chat.repositories.ChatRepo;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/chats")
public class ChatController {
    private ChatRepo chatRepo;
    private AuthRepo authRepo;

    @GetMapping({"","/"})
    public ResponseEntity<?> getAll(){
        if(authRepo.existsByToken(token)) {
            return ResponseEntity.ok(chatRepo.findAll());
        }
        return ResponseEntity.ok("You Should login first!");

    }

    @PostMapping("/create")
    public ResponseEntity<?> createChat(@RequestBody Chat chat, @RequestHeader("token") String token){
        if(authRepo.existsByToken(token)) {
            chatRepo.save(chat);
            return ResponseEntity.ok("Chat created");
        }
        return ResponseEntity.ok("You Should login first!");
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateChat(@RequestBody Chat newChat, @PathVariable("id") Long id, @RequestHeader("token") String token){
        if(authRepo.existsByToken(token)) {
            newChat.setId(id);
            chatRepo.save(newChat);

            return ResponseEntity.ok("Chat updated");
        }
        return ResponseEntity.ok("You Should login first!");

    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteChat(@PathVariable("id") Long id, @RequestHeader("token") String token){
        if(authRepo.existsByToken(token)) {
            chatRepo.deleteById(id);
            return ResponseEntity.ok("Chat deleted");
        }
        return ResponseEntity.ok("You Should login first!");
    }
}
