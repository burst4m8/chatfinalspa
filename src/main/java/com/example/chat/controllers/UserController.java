package com.example.chat.controllers;

import com.example.chat.models.User;
import com.example.chat.repositories.AuthRepo;
import com.example.chat.repositories.UserRepo;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/users")
public class UserController {
    private UserRepo userRepo;
    private AuthRepo authRepo;

    @GetMapping({"","/"})
    public ResponseEntity<?> getAll(){
        if(authRepo.existsByToken(token)) {
            return ResponseEntity.ok(userRepo.findAll());
        }
        return ResponseEntity.ok("You Should login first!");
    }

    @PostMapping("/create")
    public ResponseEntity<?> createUser(@RequestBody User user){
        if(authRepo.existsByToken(token)) {
            userRepo.save(user);

            return ResponseEntity.ok("User created");
        }
        return ResponseEntity.ok("You Should login first!");
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateUser(@RequestBody User newUser, @PathVariable("id") Long id){
        if(authRepo.existsByToken(token)) {
            newUser.setId(id);
            userRepo.save(newUser);

            return ResponseEntity.ok("User updated");

        }
        return ResponseEntity.ok("You Should login first!");
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") Long id){
        if(authRepo.existsByToken(token)) {
            userRepo.deleteById(id);

            return ResponseEntity.ok("User deleted");
        }
        return ResponseEntity.ok("You Should login first!");
    }
}
