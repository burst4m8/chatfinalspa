package com.example.chat.controllers;

import com.example.chat.models.Auth;
import com.example.chat.models.Token;
import com.example.chat.models.User;
import com.example.chat.repositories.AuthRepo;
import com.example.chat.repositories.UserRepo;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/auth")
public class AuthController{
    private AuthRepo authRepo;
    private UserRepo userRepo;

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody Auth auth){
        List<Auth> authList = authRepo.findAll();
        for(Auth a: authList){
            if (a.getLogin() == auth.getLogin() && a.getPassword == auth.getPassword){
                return ResponseEntity.ok(a.getToken());
            }
        }
        return ResponseEntity.ok("No such user found");
    }

    @PostMapping("/checkLogin")
    public ResponseEntity<?> checkLogin(@RequestHeader("token") String token){
        List<Auth> authList = authRepo.findAll();
        for(Auth a: authList){
            if (a.getToken() == token){
                return ResponseEntity.ok(a);
            }
        }
        return ResponseEntity.ok("No such user found");
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody Auth auth, @RequestBody User user){

        Token token = new Token();
        auth.setToken(token.createToken());
        authRepo.save(auth);
        userRepo.save(user);

        return ResponseEntity.ok("User registered now");
    }
}
