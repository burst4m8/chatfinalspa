package com.example.chat.controllers;

import com.example.chat.models.Messages;
import com.example.chat.repositories.AuthRepo;
import com.example.chat.repositories.MessagesRepo;

import lombok.AllArgsConstructor;
import org.hibernate.type.TrueFalseType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/messages")
public class MessagesController {
    private MessagesRepo messagesRepo;
    private AuthRepo authRepo;

    @GetMapping({"","/"})
    public ResponseEntity<?> getAll(){
        if(authRepo.existsByToken(token)) {
            return ResponseEntity.ok(messagesRepo.findAll());
        }
        return ResponseEntity.ok("You Should login first!");
    }

    @PostMapping("/create")
    public ResponseEntity<?> createMessage(@RequestBody Messages message, @RequestHeader("token") String token){
        if(authRepo.existsByToken(token)) {
            messagesRepo.save(message);

            return ResponseEntity.ok("Message created");
        }
        return ResponseEntity.ok("You Should login first!");
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateMessage(@RequestBody Messages newMessage, @PathVariable("id") Long id, @RequestHeader("token") String token){
        if(authRepo.existsByToken(token)) {
            messagesRepo.save(newMessage);

            return ResponseEntity.ok("Message updated");
        }
        return ResponseEntity.ok("You Should login first!");
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteMessage(@PathVariable("id") Long id, @RequestHeader("token") String token){
        if(authRepo.existsByToken(token)) {
            messagesRepo.deleteById(id);
            return ResponseEntity.ok("Message deleted");
        }
        return ResponseEntity.ok("You Should login first!");

    }
}
