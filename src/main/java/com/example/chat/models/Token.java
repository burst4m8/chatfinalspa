package com.example.chat.models;

import java.util.UUID;

public class Token {
    public String createToken(){
        UUID uuid = UUID.randomUUID();
        String token = uuid.toString();
        return token;
    }
}
